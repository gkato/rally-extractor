# encoding: utf-8
module RallyQuery

  SITES = ["Casa", "Bravo!", "viajeaqui", "Você RH", "Gloss", "Bebe.com", "Almanaque", "Contigo", "Recreio", "pensoumulher", "Quatro Rodas", "Claudia", "Elle", "Mundo Estranho", "Abril Videos", "Diretoria Digital", "Saude", "modaspot", "M de Mulher", "Planeta Sustentável", "Manequim", "Playboy", "Habla", "Info", "Sou + EU", "Placar", "Abril.com", "vida simples", "ViajeAqui"]

  ALX =  ["Casa", "Bravo!", "viajeaqui", "Você RH", "Gloss", "Bebe.com", "Almanaque", "Recreio", "pensoumulher", "Quatro Rodas", "Claudia", "Elle", "Mundo Estranho", "Abril Videos", "Diretoria Digital",  "Habla", "ViajeAqui"]

  NPA = ["Saude", "M de Mulher", "Planeta Sustentável", "Manequim", "Sou + EU", "Abril.com", "vida simples"]

  OUTROS = ["modaspot", "Playboy", "Info", "Placar", "ViajeAqui"]

  TYPE = ["Execução Serviço", "Incidente", "Melhoria"]

  def do_query(rally)
    query = RallyAPI::RallyQuery.new
    query.type = "hierarchicalrequirement"
    query.query_string = "(Project = https://rally1.rallydev.com/slm/webservice/1.42/project/10093146587.js)"
    query.fetch = "Name,FormattedID,Iteration,Tags,PlanEstimate"

    rally.find(query)
  end

  def grouped_by_iteration(result, iterations)
    sites = {}

    SITES.each do |site|
      planestimate = []
      iterations.each do |iter|
        planestimate << get_point_per_iteration(result, iter, site)
      end
      sites[site] = planestimate
    end
    sites.reject { |site, planestimate| planestimate.inject(:+) == 0 }
  end

  def get_point_per_iteration(result, iteration, tag)
    points = 0
    result.each do |story|
      next unless story.rally_object["Iteration"]
      if story.rally_object["Iteration"]["_ref"] == iteration["_ref"] && belongs_to_tag(story, tag)
        points += (story.rally_object["PlanEstimate"] || 0).to_i
      end
    end
    points
  end

  def belongs_to_tag(story, tag)
    story.rally_object["Tags"].map {|tag| tag["Name"] }.include? tag
  end

  def retrieve_iterations(result)
    result.map { |story| story.rally_object["Iteration"] }.uniq.compact
  end

  def retrieve_tags(result)
    result.map { |story| story.rally_object["Tags"] }.compact.flatten.uniq { |tag| tag["Name"]}
  end

  def retrieve_planestimate_by_criteria(rally, iteration, criteria={})
    query = RallyAPI::RallyQuery.new
    query.type = "hierarchicalrequirement"
    query.query_string = "(Iteration = #{iteration})"
    query.fetch = "Name,FormattedID,Iteration,Tags,PlanEstimate"

    rally.find(query).each do |story|
      story.rally_object["Tags"].each do |tag|
        tag_name = tag["Name"]
        if criteria.has_key? tag_name
          criteria[tag_name] = 0 unless criteria[tag_name]
          criteria[tag_name] = criteria[tag_name] + story.rally_object["PlanEstimate"].to_i
        end
      end
    end
    criteria.map { |key,val| [key,val] if val > 0 }.compact
  end

  def planestimate_summary(planestimate)
    summary = []
    planestimate.each do |key, val|
      summary << val.inject(:+)
    end
    summary
  end

  def summary_labels(summary, planestimate)
    summary_labels = []
    labels = planestimate.map { |key,val| key }
    total = summary.inject(:+)

    summary.size.times do |i|
      per = 100*summary[i]/total
      per = "< 1%" if per == 0
      summary_labels << "#{labels[i]} (#{summary[i]} / #{per}%)"
    end
    summary_labels
  end

  def summary_by_work_type(result, iterations)
    types = Hash[*TYPE.map {|t| [t,0] }.flatten]
    types["outros"] = 0

    result.each do |story|
      next unless story.rally_object["Iteration"]
      if belongs_to_some_iteration(story, iterations)

        found_type = false
        TYPE.each do |type|
          if belongs_to_tag(story, type)
            types[type] += story.rally_object["PlanEstimate"].to_i
            found_type = true
          end
        end

        unless found_type
          types["outros"] += story.rally_object["PlanEstimate"].to_i
          tags = story.rally_object["Tags"].map {|tag| tag["Name"] }.to_s
          puts "OUTROS em #{story.FormattedID}: #{tags}"
        end
      end
    end
    types
  end

  def work_type_label(types)
    total = types.map {|key, val| val }.inject(:+)
    labels = []
    types.each do |key, val|
      per = 100*val/total
      per = "< 1%" if per == 0
      labels << "#{key} (#{val} / #{per}%)"
    end
    labels
  end

  def belongs_to_some_iteration(story, iterations)
    iters = iterations.map { |iter| iter["_ref"] }
    iters.include? story.rally_object["Iteration"]["_ref"]
  end

  def ordered_summary(planestimate)
   sites = []
   effort = []
   @planestimate.map {|k, v| {k => v.inject(:+)} }.sort { |a,b| a.first[1] <=> b.first[1] }.reverse.each do |site|
     sites << site.first[0]
     effort << site.first[1]
   end
   {:sites => sites, :effort => effort}
  end

  def by_cms_type(summary, summary_labels)
    by_cms_type = {:alexandria => 0, :npa => 0, :outros => 0}

    summary.each_with_index do |s, i|
      site = summary_labels[i].gsub(/\(.*?\)/, "").strip
      by_cms_type[:alexandria] += s if ALX.include? site
      by_cms_type[:npa] += s if NPA.include? site
      by_cms_type[:outros] += s if OUTROS.include? site
    end

    total = by_cms_type.values.inject(:+)

    labels = []
    by_cms_type.keys.map { |key| key.to_s.capitalize }.each_with_index do |site, i|
      val = by_cms_type.values[i]
      per = 100*val/total rescue 0
      per = "< 1" if per == 0
      labels << "#{site} (#{val} / #{per}%)"
    end

    tmp = by_cms_type.clone
    tmp.keys.each_with_index do |key, i|
      by_cms_type[labels[i]] = by_cms_type[key]
      by_cms_type.delete(key)
    end
    by_cms_type
  end
end
