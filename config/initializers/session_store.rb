# Be sure to restart your server when you modify this file.

RallyExtractor::Application.config.session_store :cookie_store, :key => '_rally_extractor_session'

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# RallyExtractor::Application.config.session_store :active_record_store
