require 'rally_api'
require 'rally_query'

class RallyExtractionsController < ApplicationController

  include RallyQuery

  before_filter :auth, :only => [:index, :planestimate]

  def index
    qt_iterations = (params["qt_iterations"] || 4).to_i
    @pie_type = params["pie_type"] || "pie"

    query = do_query(@rally)
    @iterations = retrieve_iterations(query).last(qt_iterations)
    @planestimate = grouped_by_iteration(query, @iterations)
    @summary = planestimate_summary(@planestimate)
    @summary_labels = summary_labels(@summary, @planestimate)
    @work_type = summary_by_work_type(query, @iterations)
    @work_type_labels = work_type_label(@work_type)
    @ordered = ordered_summary(@planestimate)
    @by_cms_type = by_cms_type(@summary, @summary_labels)
  end

  def planestimate
    iteration = params["iteration"]
    tags = params["tags"]
    criteria = Hash[*tags.map { |tag| [tag, 0] }.flatten]
    @iteration_name = params["iteration_name"]
    @planestimate = retrieve_planestimate_by_criteria(@rally, iteration, criteria)
  end

  def login
  end

  private
  def auth
    session[:user] = params[:user] if params[:user]
    session[:pass] = params[:pass] if params[:pass]

    @rally ||= rally_login if (session[:user] && session[:pass])

    redirect_to(:action => :login) unless @rally
  end

  def rally_login
    config = {:base_url => "https://rally1.rallydev.com/slm"}
    config[:username] = session[:user]
    config[:password] = session[:pass]
    RallyAPI::RallyRestJson.new(config) rescue nil
  end
end
